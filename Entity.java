import java.awt.*;

public class Entity {
    public Game game;
    public double x;
    public double y;
    public int[][] map;

    public Entity(Game game, double x, double y) {
        this.game = game;
        this.x = x;
        this.y = y;
        map = game.getMap();
    }
}
