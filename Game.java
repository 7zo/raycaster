import java.awt.*;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.util.Random;

import javax.swing.*;

public class Game implements Runnable {

    private String title;
    private int width;
    private int height;

    private boolean linux;
    
    private JFrame frame;
    private Canvas canvas;

    private Input input;

    private BufferStrategy bs;

    private Thread thread;
    private boolean running = false;

    int miniMapScale = 10;
    public int[][] miniMap =
            {
                    {1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2},
                    {1, 0, 0, 0, 9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
                    {1, 0, 3, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 2},
                    {1, 0, 3, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 2},
                    {1, 0, 3, 0, 0, 0, 3, 0, 2, 2, 2, 0, 2, 2, 0, 2},
                    {1, 0, 3, 0, 0, 0, 3, 0, 2, 0, 0, 0, 0, 0, 0, 2},
                    {1, 0, 3, 3, 0, 3, 3, 0, 2, 0, 0, 0, 0, 0, 0, 2},
                    {1, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 2},
                    {1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 4, 0, 4, 4, 0, 2},
                    {1, 0, 0, 0, 0, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0, 2},
                    {1, 0, 0, 0, 0, 0, 1, 4, 0, 0, 0, 0, 0, 1, 0, 2},
                    {1, 0, 0, 0, 0, 0, 1, 4, 0, 3, 3, 3, 3, 0, 0, 2},
                    {1, 0, 0, 0, 0, 0, 1, 4, 0, 3, 3, 3, 3, 0, 0, 2},
                    {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2},
                    {1, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 4, 4, 2}
            };


    private int cellSize = 10;

    private int[][] map = multiplyMap(miniMap, cellSize);

    private BufferedImage mapImage = mapImage = getMinimap(miniMap);

    private Player player;

    private Color color;

    public Game(String title, int width, int height) {
        this.title = title;
        this.width = width;
        this.height = height;

        frame = new JFrame();
        canvas = new Canvas();
        input = new Input();

        player = new Player(this, cellSize, cellSize);

        canvas.setPreferredSize(new Dimension(width, height));
        canvas.setMinimumSize(new Dimension(width, height));
        canvas.setMaximumSize(new Dimension(width, height));
        canvas.setFocusable(false);
        canvas.setBackground(Color.BLACK);

        frame.setResizable(false);
        frame.setSize(width, height);
        frame.setTitle(title);
        frame.add(canvas);
        frame.pack();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.addKeyListener(input);
        
        linux = System.getProperty("os.name").startsWith("Linux");
    }

    public void run() {
        int ticks = 0;
        int frames = 0;

        double tps = 60.0;
        double nsPerTick = 1000000000.0 / tps;
        long lastTime = System.nanoTime();
        long now;
        long timer = 0;
        double delta = 0;

        while (running) {
            now = System.nanoTime();
            delta += (now - lastTime) / nsPerTick;
            timer += now - lastTime;
            lastTime = now;
            if (delta >= 1) {
                tick();
                ticks++;
                delta--;
            }

            render();
            frames++;
            if (timer >= 1000000000) {
                System.out.println(ticks + " tps, " + frames + " fps");
                frame.setTitle(title + "  |  " + ticks + " tps, " + frames + " fps");
                ticks = 0;
                frames = 0;
                timer = 0;
            }
        }
        stop();
    }

    public void tick() {
        input.tick();
        player.tick();
    }

    public void render() {

        bs = canvas.getBufferStrategy();
        if (bs == null) {
            canvas.createBufferStrategy(3);
            return;
        }
        Graphics g = bs.getDrawGraphics();

        // Clear

        g.setColor(Color.BLACK);
        g.fillRect(0, 0, width, height);

        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, width, height / 2);

        // Draw

        double angleBetweenRays = Math.toRadians(0.5);
        double fov = Math.toRadians(80);
        int rays = (int) (fov / angleBetweenRays);
        //int rays = (int) Math.round((fov/angleBetweenRays) / 2) * 2;
        int lineW = width / rays;

        double angle = 0;

        for (int i = -rays / 2; i < rays / 2; i++) {

            angle = player.angle + angleBetweenRays * i;

            if (angle < 0) angle += 2 * Math.PI;
            if (angle > 2 * Math.PI) angle -= 2 * Math.PI;

            int wallValue = getWallValue(player.x, player.y, angle);

            double distance = Math.hypot(getWall(player.x, player.y, angle).x - player.x, getWall(player.x, player.y, angle).y - player.y);

            double ca = player.angle - angle;
            if (ca < 0) ca += 2 * Math.PI;
            if (ca > 2 * Math.PI) ca -= 2 * Math.PI;
            distance *= Math.cos(ca);

            int lineH = (int) ((cellSize * height) / distance);
            if (lineH > height) lineH = height;
            int lineO = height / 2 - lineH / 2;

            /*
            if (isVerticalWall(getWall(player.x, player.y, angle).x, getWall(player.x, player.y, angle).y)) {
                color = Color.LIGHT_GRAY;
            } else {
                color = Color.GRAY;
            }
            */

            if (wallValue == 1) color = Color.LIGHT_GRAY;
            else if (wallValue == 2) color = Color.GRAY;
            else if (wallValue == 3) color = Color.DARK_GRAY;
            else if (wallValue == 4) color = Color.WHITE;
            else color = Color.RED;

            //color = Color.LIGHT_GRAY;
            double darknessScale = 3;

            //Random random = new Random();
            //int red = (int) (random.nextInt(256) - distance * darknessScale);
            //int green = (int) (random.nextInt(256) - distance * darknessScale);
            //int blue = (int) (random.nextInt(256) - distance * darknessScale);

            int red = (int) (color.getRed() - distance * darknessScale);
            int green = (int) (color.getGreen() - distance * darknessScale);
            int blue = (int) (color.getBlue() - distance * darknessScale);
            if (red < 0) red = 0;
            if (red > 255) red = 255;
            if (green < 0) green = 0;
            if (green > 255) green = 255;
            if (blue < 0) blue = 0;
            if (blue > 255) blue = 255;
            g.setColor(new Color(red, green, blue));

            g.fillRect((lineW * i) + width / 2, lineO, lineW, lineH);
        }

        g.drawImage(mapImage, 0, 0, mapImage.getWidth() * miniMapScale, mapImage.getHeight() * miniMapScale, null);
        g.setColor(Color.BLUE);
        g.drawLine((int) (player.x / cellSize * miniMapScale), (int) (player.y / cellSize * miniMapScale), (int) (player.xDir * 10 + player.x / cellSize * miniMapScale), (int) (player.yDir * 10 + player.y / cellSize * miniMapScale));
        g.fillRect((int) (player.x / cellSize * miniMapScale - 2), (int) (player.y / cellSize * miniMapScale - 2), 4, 4);
        //g.drawLine((int) player.x, (int) player.y, getWall(player.x, player.y, player.angle).x, getWall(player.x, player.y, player.angle).y);

        /*
        for (int i = -rays / 2; i < rays / 2; i++) {
            angle = player.angle + angleBetweenRays * i;
            g.setColor(Color.BLUE);
            g.drawLine((int) player.x, (int) player.y, getWall(player.x, player.y, angle).x, getWall(player.x, player.y, angle).y);
        }
        */

        //g.setColor(Color. RED);
        //g.drawString("x: " + player.x, 10, 170);
        //g.drawString("y: " + player.y, 10, 180);
        //g.drawString("angle: " + player.angle, 10, 190);

        // End

        bs.show();
        if (linux) {
            Toolkit.getDefaultToolkit().sync();
        }
        g.dispose();
    }

    public synchronized void start() {
        if (running)
            return;
        running = true;
        thread = new Thread(this);
        thread.start();
    }

    public synchronized void stop() {
        if (!running)
            return;
        running = false;
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Input getInput() {
        return input;
    }

    public int[][] getMap() {
        return map;
    }

    public BufferedImage getMinimap(int[][] map) {
        BufferedImage image = new BufferedImage(map[0].length, map.length, BufferedImage.TYPE_INT_RGB);
        int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
        int count = 0;
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                if (map[i][j] != 0) {
                    pixels[count] = Color.RED.getRGB();
                } else {
                    pixels[count] = Color.WHITE.getRGB();
                }
                count++;
            }
        }
        return image;
    }

    public int[][] multiplyMap(int[][] map, int cellSize) {
        int[][] newMap = new int[cellSize * map.length][cellSize * map[0].length];
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[0].length; j++) {
                for (int k = 0; k < cellSize; k++) {
                    for (int f = 0; f < cellSize; f++) {
                        newMap[(cellSize * i) + k][(cellSize * j) + f] = map[i][j];
                    }
                }
            }
        }
        return newMap;
    }

    public Point getWall(double x, double y, double angle) {
        double newXDir = Math.cos(angle);
        double newYDir = Math.sin(angle);

        while (map[(int) y][(int) x] == 0) {
            x += newXDir;
            y += newYDir;
        }
        return new Point((int) x, (int) y);
    }

    public int getWallValue(double x, double y, double angle) {
        double newXDir = Math.cos(angle);
        double newYDir = Math.sin(angle);

        while (map[(int) y][(int) x] == 0) {
            x += newXDir;
            y += newYDir;
        }
        return map[(int) y][(int) x];
    }

    /*
    public boolean isNorthWall(int x, int y) {
        if (y + 1 > map[y].length) return false;
        if (map[y + 1][x] == 0) return true;
        return false;
    }

    public boolean isEastWall(int x, int y) {
        if (x - 1 < 0) return false;
        if (map[y][x - 1] == 0) return true;
        return false;
    }

    public boolean isSouthWall(int x, int y) {
        if (y - 1 < 0) return false;
        if (map[y - 1][x] == 0) return true;
        return false;
    }

    public boolean isWestWall(int x, int y) {
        if (x + 1 > map[y].length) return false;
        if (map[x + 1][x] == 0) return true;
        return false;
    }

    public boolean isVerticalWall(int x, int y) {
        if (isNorthWall(x, y) || isSouthWall(x, y)) return true;
        return false;
    }
    */
}
